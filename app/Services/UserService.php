<?php


namespace App\Services;


use App\Http\Resources\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;

class UserService extends BaseService
{
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct($userRepository, User::class);
    }

    public function saveUserImage($userId, UploadedFile $file) {
        $extension = $file->getClientOriginalExtension();
        $now = Carbon::now()->timestamp;
        $fileName = "{$now}{$userId}.{$extension}";
        $file->storeAs("/user/{$userId}", $fileName);
        return $this->update($userId, [ "image" => $fileName ]);
    }

    public function updateUserPassword($userInfo, $originPassword, $newPassword)
    {
        $user = $this->searchById($userInfo["id"]);
        $isManger = $userInfo['role'] === "manager" || $userInfo['role'] === "proxy";
        $isSelf = Hash::check($originPassword, $user->password);
        if ($isManger || $isSelf) {
            $data = [ "password" => Hash::make($newPassword) ];
            return $this->update($user->id, $data);
        }
        throw new \Exception('actionWithoutPermissionError', 20203050104);
    }

    public function deleteUser($userInfo, $password)
    {
        $user = $this->searchById($userInfo["id"]);
        $isManger = $userInfo['role'] === "manager" || $userInfo['role'] === "proxy";
        $isSelf = Hash::check($password, $user->password);
        if ($isManger || $isSelf) {
            $this->delete($user->id);
        }
        throw new \Exception('actionWithoutPermissionError', 20203050104);
    }

    public function userInfoInRequest (FormRequest $request)
    {
        $result = [ "role" => null, "id" => 0 ];
        $loginUser = auth()->user();
        $isManager = $loginUser->isManager();
        $isUser = $loginUser->isUser();
        if ($isUser) {
            // if the role (type) of login user is simple user, can only get/set self's information
            $result["role"] = "user";
            $result["id"] = $loginUser->id;
        } else if ($isManager) {
            // manager has two cases
            // 01. other people's ID => must have "userId" property in request
            // 02. self's ID
            $requestHasUserIdProperty = $request->has("userId") && isset($request["userId"]) && !is_null($request["userId"]);
            if ($requestHasUserIdProperty) {
                $result["role"] = "proxy";
                $result["id"] = $request["userId"];
            } else {
                $result["role"] = "manager";
                $result["id"] = $loginUser->id;
            }
        }
        return $result;
    }

    public function hasPermission($userInfo, $targetId)
    {
        return true;
    }
}
