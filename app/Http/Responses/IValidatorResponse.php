<?php

namespace App\Http\Responses;


interface IValidatorResponse
{
    /**
     * Update properties for FormRequest validation error
     *
     * @param \Illuminate\Support\MessageBag $error
     */
    public function setValidationError($error);

    /**
     * Get a JsonResponse with properties
     *
     * @param int $httpStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function toJSONResponse($httpStatus = 422);
}