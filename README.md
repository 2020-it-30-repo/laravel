## 處方箋
使用之前請依下列步驟進行：

01. 透過 composer 安裝套件

02. 在 DBMS 加入 `2020-it-30` 資料庫

03. 執行 migrate

04. 在 `httpd-vhosts.conf` 加入下列設定 (檔案路徑要換成對的)
```
<VirtualHost *:80>
  ServerName 2020-it-30-api.org
  ServerAlias 2020-it-30-api.org
  DocumentRoot "\2020-it-30-laravel\public"
  <Directory "\2020-it-30-laravel\public">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
</VirtualHost>
```

05. 重啟 wamp 服務