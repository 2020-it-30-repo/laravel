<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "role" => $this->roleToString($this->role),
            "isDeleted" => !!$this->deleted_at,
            "createdAt" => $this->created_at,
            "updatedAt" => $this->updated_at,
        ];
    }

    private function roleToString($role) {
        switch ($role) {
            default:
                return "error";
            case 0:
                return "manager";
            case 1:
                return "user";
        }
    }
}
