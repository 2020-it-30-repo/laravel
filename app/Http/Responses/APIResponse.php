<?php


namespace App\Http\Responses;


class APIResponse implements IValidatorResponse
{
    private $code;

    private $status;

    private $data;

    private $message;

    private $httpStatus = 200;

    public function __constructor()
    {
    }

    public function isSuccess() {
        $this->code = 200;
        $this->status = true;
        $this->httpStatus = 200;
        return $this;
    }

    public function isFailed() {
        $this->code = 500;
        $this->status = false;
        $this->httpStatus = 422;
        return $this;
    }

    public function setCode($code) {
        if (!is_null($code)) {
            $this->code = $code;
        }
        return $this;
    }

    public function setStatus($status) {
        if (!is_null($status)) {
            $this->status = $status;
        }
        return $this;
    }

    public function setData($data) {
        if (!is_null($data)) {
            $this->data = $data;
        }
        return $this;
    }

    public function setMessage($message) {
        if (!is_null($message)) {
            $this->message = $message;
        }
        return $this;
    }

    /**
     * Update properties for FormRequest validation error
     * @param \Illuminate\Support\MessageBag $error
     * @return $this
     */
    public function setValidationError($error)
    {
        $this->isFailed();
        $this->setData($error);
        return $this;
    }

    /**
     * Get an array with properties
     *
     * @return array
     */
    public function toArray() {
        return [
            "code" => $this->code,
            "status" => $this->status,
            "data" => $this->data,
            "message" => $this->message,
        ];
    }

    /**
     * Get a JsonResponse with properties
     *
     * @param int $httpStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function toJSONResponse($httpStatus = 0)
    {
        $status = ($httpStatus !== 0) ? $httpStatus : $this->httpStatus;
        return response()->json($this->toArray(), $status);
    }
}
