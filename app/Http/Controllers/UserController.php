<?php


namespace App\Http\Controllers;


use App\helpers\Helper;
use App\Http\Requests\User\AccessUserRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\LogoutRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Responses\APIResponse;
use App\Services\UserService;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userService;
    private $helper;

    public function __construct(UserService $userService, Helper $helper)
    {
        $this->userService = $userService;
        $this->helper = $helper;
    }

    // API

    public function login(LoginRequest $request, APIResponse $response)
    {
        // data pre-process
        $userData = $request->only([ 'email', 'password' ]);

        try {
            $token = auth()->attempt($userData);
            if ($token) {
                $user = auth()->user();
                $user = $this->userService->modelInCamelCase($user);
                $response->isSuccess()->setData([ "token" => $token, "user" => $user ]);
            } else {
                throw new \Exception('loginError', 20203050201);
            }
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function accessUser(AccessUserRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo["id"];

        try {
            $user = $this->userService->searchById($userId);
            $user = $this->userService->modelInCamelCase($user);
            $response->isSuccess()->setData($user);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function createUser(CreateUserRequest $request, APIResponse $response)
    {
        $this->create($request, 1, $response);
        return $response->toJSONResponse();
    }

    public function createManager(CreateUserRequest $request, APIResponse $response) {
        $this->create($request, 0, $response);
        return $response->toJSONResponse();
    }

    public function updateUser(UpdateUserRequest $request, APIResponse $response)
    {
        $userInfo = $this->userService->userInfoInRequest($request);
        $userId = $userInfo["id"];
        $userData = $request->only([ 'name', 'email', 'phone' ]);

        try {
            $user = $this->userService->update($userId, $userData);
            $user = $this->userService->modelInCamelCase($user);
            $response->isSuccess()->setData($user);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
        return $response->toJSONResponse();
    }

    public function logout(LogoutRequest $request, APIResponse $response) {
        auth()->logout();
        return $response->isSuccess()->toJSONResponse();
    }

    public function jwtTokenAuthError(APIResponse $response)
    {
        return $response->isFailed()
            ->setCode(20203040001)
            ->setMessage('authValidationError')
            ->toJSONResponse();
    }


    // private
    private function create(CreateUserRequest $request, $role, APIResponse &$response)
    {
        $newUserData = $request->only([ 'name', 'email', 'password' ]);
        $newUserData['password'] = Hash::make($newUserData['password']);
        $newUserData['role'] = $role;

        try {
            $newUser = $this->userService->create($newUserData);
            $newUser = $this->userService->modelInCamelCase($newUser);
            $response->isSuccess()->setData($newUser);
        } catch (\Exception $exception) {
            $this->helper->setFailedAPIResponseWithException($response, $exception);
        }
    }
}
