<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/14
 * Time: 下午 05:22
 */

namespace App\Services;


use App\helpers\OrderInfo;
use App\helpers\PageInfo;
use App\Repositories\IRepository;
use Illuminate\Support\Collection;

abstract class BaseService implements IService
{
    protected $repository;
    protected $resourceClass;

    public function __construct(IRepository $repository, $resourceClass)
    {
        $this->repository = $repository;
        $this->resourceClass = $resourceClass;
    }

    public function create(array $data)
    {
        return $this->repository->create($data);
    }

    public function update($id, array $data, $checkPermission = false, $userInfo = [])
    {
        $hasPermission = $this->hasPermission($userInfo, $id);
        if ($hasPermission || !$checkPermission) {
            return $this->repository->update($id, $data);
        }
        throw new \Exception('actionWithoutPermissionError', 20203050101);
    }

    public function delete($id, $checkPermission = false, $userInfo = [])
    {
        $hasPermission = $this->hasPermission($userInfo, $id);
        if ($hasPermission || !$checkPermission) {
            $this->repository->delete($id);
        } else {
            throw new \Exception('actionWithoutPermissionError', 20203050101);
        }
    }

    public function searchById($id, $checkPermission = false, $userInfo = [], $exceptionOnNull = true)
    {
        $hasPermission = $this->hasPermission($userInfo, $id);
        if ($hasPermission || !$checkPermission) {
            return $this->repository->searchModelById($id, $exceptionOnNull);
        }
        throw new \Exception('actionWithoutPermissionError', 20203050101);
        
    }

    public function search(array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = ['*'])
    {
        return $this->repository->searchModels($conditions, $pageInfo, $orderInfo, $columns);
    }

    public function count(array $conditions = [])
    {
        return $this->repository->count($conditions);
    }

    public function modelInCamelCase($model)
    {
        if ($model) {
            return new $this->resourceClass($model);
        } else {
            throw new \Exception('transModelInCamelCaseError', 20203050102);
        }
    }

    public function collectionInCamelCase(Collection $modelCollection)
    {
        try {
            return $this->resourceClass::collection($modelCollection);
        } catch (\Exception $ex) {
            throw new \Exception('transModelCollectionInCamelCaseError', 20203050103);
        }
    }

    public function toDictionary(Collection $modelCollection, string $property = 'id', bool $groupInArray = false)
    {
        $result = [];
        foreach ($modelCollection as $model) {
            $resultKey = $model[$property];
            $isEmpty = empty($result[$resultKey]);
            $value = $this->modelInCamelCase($model);
            if ($groupInArray) {
                if ($isEmpty) {
                    $result[$resultKey] = [];
                }
                array_push($result[$resultKey], $value);
            } else {
                $result[$resultKey] = $value;
            }
        }
        return $result;
    }

    public abstract function hasPermission($userInfo, $targetId);
}
