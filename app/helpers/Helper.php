<?php
/**
 * Created by PhpStorm.
 * User: Albert Lin
 * Date: 2019/8/15
 * Time: 上午 11:28
 */

namespace App\helpers;

use App\Http\Responses\APIResponse;


class Helper
{
    public function __construct()
    {
    }

    public function setFailedAPIResponseWithException(APIResponse &$response, \Exception $exception, array $skipExceptionCodes = [])
    {
        $errorMessage = $exception->getMessage();
        $errorCode = $exception->getCode();
        $toSkip = in_array((int)$errorCode, $skipExceptionCodes);
        if ($toSkip) {
            $response->isSuccess();
        } else {
            $response->isFailed()->setCode($errorCode)->setMessage($errorMessage);
        }
    }
}
