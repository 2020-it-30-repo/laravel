<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix("v1")->group(function() {
    // login
    Route::post("login", "UserController@login");
    Route::post('register', 'UserController@createUser');
    Route::get('auth-error', 'UserController@jwtTokenAuthError')->name('api-auth-failed');

    Route::middleware(['auth:api', 'role:both'])->group(function () {
        Route::prefix("user")->group(function () {
            Route::post('/', 'UserController@accessUser');
            Route::post('/create', 'UserController@createManager')->middleware('role:manager');
            Route::post('/update', 'UserController@updateUser');
            Route::post('/logout', 'UserController@logout');
        });
    });
});
