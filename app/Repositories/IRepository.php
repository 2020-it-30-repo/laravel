<?php


namespace App\Repositories;

use App\helpers\OrderInfo;
use App\helpers\PageInfo;

interface IRepository
{

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function searchModelById($id, $exceptionWithNull = false);

    public function searchModels(array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = array('*'));

    public function count(array $conditions = []);
}
