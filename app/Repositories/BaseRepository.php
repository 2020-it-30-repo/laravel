<?php


namespace App\Repositories;


use App\helpers\OrderInfo;
use App\helpers\PageInfo;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements IRepository
{
    protected $app;
    protected $modelClass;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->modelClass = $this->modelClass();
    }

    protected abstract function modelClass();

    public function create(array $data)
    {
        $modelInstance = resolve($this->modelClass);
        $createSuccess = $this->saveModel($modelInstance, $data);
        if ($createSuccess) {
            return $modelInstance;
        }
        throw new \Exception('createModelError', 20203050001);
    }

    public function update($target, array $data)
    {
        $modelInstance = $this->initModelInstance($target);
        $updatedSuccess = $this->saveModel($modelInstance, $data);
        if ($updatedSuccess) {
            return $modelInstance;
        }
        throw new \Exception('updateModelError', 20203050002);
    }

    public function delete($target)
    {
        $modelInstance = $this->initModelInstance($target);
        $deleteSuccess = $this->modelClass::destroy($modelInstance->id);
        if (!$deleteSuccess) {
            throw new \Exception('deleteModelError', 20203050003);
        }
    }

    public function searchModelById($id, $exceptionOnNull = true)
    {
        $modelInstance = $this->modelClass::find($id);
        if ($modelInstance || !$exceptionOnNull) {
            return $modelInstance;
        }
        throw new \Exception('selectNoResultError', 20203050004);
    }

    public function searchModels(array $conditions = [], PageInfo $pageInfo = null, OrderInfo $orderInfo = null, array $columns = array('*'))
    {
        $query = $this->initQuery()->where($conditions);

        if ($pageInfo) {
            $query->offset($pageInfo->offset)->limit($pageInfo->size);
        }

        if ($orderInfo) {
            $query->orderBy($orderInfo->column, $orderInfo->orientation);
        }

        return $query->get($columns);
    }

    public function count(array $conditions = []) {
        return $this->initQuery()->where($conditions)->count();
    }

    protected function initModelInstance($target)
    {
        $isInstance = $target && $target instanceof $this->modelClass;
        $isId = $target && is_int($target);
        if ($isInstance) {
            return $target;
        } else if ($isId) {
            return $this->searchModelById($target, true);
        }
        throw new \Exception('initModelError', 20203050005);
    }

    protected function saveModel(&$modelInstance, array $data)
    {
        if (isset($modelInstance)) {
            foreach($data as $key => $value) {
                if (isset($value)) {
                    $modelInstance[$key] = $value;
                }
            }
            return $modelInstance->save();
        }
        return false;
    }

    protected function initQuery() {
        try {
            return $this->modelClass::withTrashed();
        } catch (\Exception $ex) {
            return $this->app->make($this->modelClass);
        }
    }
}
