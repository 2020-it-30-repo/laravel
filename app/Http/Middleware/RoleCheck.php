<?php

namespace App\Http\Middleware;

use App\helpers\Helper;
use App\Http\Responses\APIResponse;
use Closure;

class RoleCheck
{

    private $helper;
    private $response;

    public function __construct(Helper $helper, APIResponse $response)
    {
        $this->helper = $helper;
        $this->response = $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param String $role
     * @return mixed
     */
    public function handle($request, Closure $next, string $role)
    {
        $user = auth()->user();
        $correctRole = false;
        $isManager = $user->isManager();
        $isUser = $user->isUser();
        switch ($role) {
            case "both":
                $correctRole = $isManager || $isUser;
                break;
            case "manager":
                $correctRole = $isManager;
                break;
            case "user":
                $correctRole = $isUser;
                break;
        }
        if ($correctRole) {
            return $next($request);
        }
        $exception = new \Exception('tokenError', 20203050301);
        $this->helper->setFailedAPIResponseWithException($this->response, $exception);
        return $this->response->toJSONResponse();
    }
}
